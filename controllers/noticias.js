
var Sequelize = require('sequelize');
var models = require("../models");
var express = require('express');
var router = express.Router();
var session = require('express-session');
var jwt = require('jsonwebtoken');
var mySecret = "2a4Hj423rmtjsOIsAAAkj";
var unirest = require('unirest');

function isInteger(value) {
  var x = parseFloat(value);
  return !isNaN(value) && (x | 0) === x;
}

var IdUsuario=-1;

var verificarAutentificacion = function(req, resp, next){
	var token=req.body.token || req.query.token || req.headers['x-access-token'];
	if (token) {
    	jwt.verify(token, mySecret, function(err, decoded) {      
		    if (err) {
		        return resp.json({ success: false, message: 'Failed to authenticate token.' });    
		    }else {
		    	IdUsuario=jwt.decode(token, mySecret).id;
				next();
		    }
		});
	} else {
    	return resp.status(403).send({ 
	        success: false, 
	        message: 'No token provided.' 
	    });
  	}
}

var verificarPertenencia = function(req, resp, next){
	var id=req.params.id;
	return models.Noticia.findById(id).then(function(noticia){
		if(noticia)
			if(IdUsuario == noticia.UsuarioId){
			  	next();
			}else{
				resp.status(403);
				//resp.send('No autorizado'+IdUsuario+' '+mensaje.de);
			}
		else{
			resp.status(404);
			resp.send('No encontrado');

		}

	})
}



// Ver mensajes de un usuario (el usuario puede ser tanto origen como destinatario )
router.get('/',verificarAutentificacion, function(req, res) {
	if(req.query.limit<1 || req.query.limit>20 || !isInteger(req.query.limit) )
		limite=20;
	else
		limite=req.query.limit;
	var offset=req.query.offset;
	if(!(isInteger(offset)))
		offset=0;
	res.status(200);

	var q=
	"SELECT DISTINCT `Noticia`.`id`, `U1`.`nombre` AS `UsuarioNombre`, `Noticia`.`texto`,`Noticia`.`UsuarioId`, `Noticia`.`titulo` , `Noticia`.`createdAt` "
	+"FROM `Noticia` , `Usuarios`"
	+" LEFT OUTER JOIN `Usuarios` AS `U1` ON `Noticia`.`UsuarioId` = `U1`.`id` "
	+" WHERE  (`Noticia`.`UsuarioId` =  `Usuarios`.`id` OR `Noticia`.`UsuarioId` =  `Usuarios`.`UsuarioId` )" 
 	+" AND `Usuarios`.`id` = "+IdUsuario+" "
	+" LIMIT "+limite+" OFFSET "+offset


	models.sequelize.query(q,{ type: Sequelize.QueryTypes.SELECT})
	.then(function(projects) {
		res.send(projects)
	})
});
 



// Añadir mensajes
router.post('/',verificarAutentificacion, function(req, res) {
	console.log(req.body);
	console.log(req.headers)
  	res.status(201);
  	req.body.UsuarioId=IdUsuario;
 	models.Noticia.create(req.body).then(function(noticia){
 		res.send(noticia);
	});

});

// Actualizar mensajes
router.put('/:id',verificarAutentificacion,verificarPertenencia, function(req, res) {
  	res.status(200);
  	models.Noticia.findById(req.params.id).then(function(noticia){
  		noticia.update(req.body).then(function(noticia){
 			res.send(noticia);
  		})
  	});
			 	
});

// Borrar mensajes
router.delete('/:id',verificarAutentificacion ,verificarPertenencia,function(req, res) {
	
	res.status(204);
  	models.Noticia.findById(req.params.id).then(function(noticia){
		noticia.destroy();
	});
	res.send('');
			
});


module.exports = router;