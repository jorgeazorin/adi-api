var Sequelize = require('sequelize');
var models = require("../models");
var express = require('express');
var router = express.Router();
var session = require('express-session');
var jwt = require('jsonwebtoken');
var mySecret = "2a4Hj423rmtjsOIsAAAkj";
var unirest = require('unirest');

function isInteger(value) {
  var x = parseFloat(value);
  return !isNaN(value) && (x | 0) === x;
}

var IdUsuario=-1;
var tipo=-1;

var verificarAutentificacion = function(req, resp, next){
	var token=req.body.token || req.query.token || req.headers['x-access-token'];
	if (token) {
    	jwt.verify(token, mySecret, function(err, decoded) {      
		    if (err) {
		        return resp.json({ success: false, message: 'Failed to authenticate token.' });    
		    }else {
		    	IdUsuario=jwt.decode(token, mySecret).id;
		    	tipo=jwt.decode(token,mySecret).tipo;
				next();
		    }
		});
	} else {
    	return resp.status(403).send({ 
	        success: false, 
	        message: 'No token provided.' 
	    });
  	}
}

var verificarAsesor = function(req, resp, next){
	if(tipo!=1){
		return resp.status(403).send({ 
	        success: false, 
	        message: 'No tienes permiso' 
	    });
	}else{
		next();
	}
}

var verificarPertenencia = function(req, resp, next){
	var id=req.params.id;
	return models.Mensaje.findById(id).then(function(mensaje){
		if(mensaje)
			if(IdUsuario == mensaje.de){
			  	next();
			}else{
				resp.status(403);
				resp.send('No autorizado'+IdUsuario+' '+mensaje.de);
			}
		else{
			resp.status(404);
			resp.send('No encontrado');

		}

	})
}

//Ver el propio usuario
router.get('/yo',verificarAutentificacion, function(req, res) {
	models.Usuario.findById(IdUsuario, {include: [{ all: true }]}).then(function(usuario){
		res.status(200);
		res.send(usuario);
	})
})



//Ver todos los clientes 

router.get('/clientes',verificarAutentificacion,verificarAsesor, function(req, res) {
	if(req.query.limit<1 || req.query.limit>20 || !isInteger(req.query.limit) )
		limite=20;
	else
		limite=req.query.limit;
	
	res.status(200);
	models.Usuario.findAll({limite,  offset: req.query.offset, where:{tipo:'2'}}).then(function(results){
		res.send(results);
	}); 
});




//Ver clientes de un Usuario
router.get('/misClientes/',verificarAutentificacion, function(req, res) {
	models.Usuario.findById(IdUsuario).then(function(usuario){
		if(usuario.tipo==1){
			usuario.getUsuarios().then(function(usuarios){
				res.status(200);
				res.send(usuarios);
			})
		}else{
			models.Usuario.findById(usuario.UsuarioId).then(function(usuario){
				res.status(200);
				res.send({usuario});
			})
		}
		
	})
	
})



//Ver un cliente de un Usuario
router.get('/clientes/:id',verificarAutentificacion,verificarAsesor, function(req, res) {
	models.Usuario.findById(req.params.id).then(function(usuario){
		res.status(200);
		res.send(usuario);
	})
})

//Agregar un Usuario
router.post('/clientes/',verificarAutentificacion,verificarAsesor, function(req, res) {
	res.status(201);
 	models.Usuario.create(req.body).then(function(usuario){
 		res.send(usuario);
	});
})


//Actualizar un Usuario
router.put('/:id',verificarAutentificacion, function(req, res) {
	if(tipo==1 || IdUsuario==req.params.id){
		res.status(200);
	  	models.Usuario.findById(req.params.id).then(function(usuario){
	  		usuario.update(req.body).then(function(usuario){
	 			res.send(usuario);
	  		})
	  	});	
	  }else{
	  	res.status(403);
	  	res.send('No tienes permiso para esto')
	  }
	
})


// Borrar usuario
router.delete('/:id',verificarAutentificacion ,verificarAsesor,function(req, res) {
	res.status(204);
  	models.Usuario.findById(req.params.id).then(function(usuario){
		usuario.destroy();
	});
	res.send('');
});


// agregar a sus clientes
router.post('/clientes/:id',verificarAutentificacion ,verificarAsesor,function(req, res) {
	models.Usuario.findById(req.params.id).then(function(cliente){
		if(cliente){
			res.status(201);
			cliente.addUsuario(cliente);
			res.send('todo ok');	
		}else{
			res.status(404)
			res.send('Cliente no encontrado');	
		}
	});
});

// eliminar de sus clientes
router.delete('/clientes/:id',verificarAutentificacion ,verificarAsesor,function(req, res) {
	models.Usuario.findById(req.params.id).then(function(cliente){
		if(cliente){
			res.status(201);
			usuario.asesor=-1;
			cliente.save();
			res.send('');	
		}else{
			res.status(404)
			res.send('Cliente no encontrado');	
		}
	});
});

module.exports = router;