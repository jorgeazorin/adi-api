
var Sequelize = require('sequelize');
var models = require("../models");
var express = require('express');
var router = express.Router();
var session = require('express-session');
var jwt = require('jsonwebtoken');
var mySecret = "2a4Hj423rmtjsOIsAAAkj";
var unirest = require('unirest');
//var email   = require("emailjs");


function isInteger(value) {
  var x = parseFloat(value);
  return !isNaN(value) && (x | 0) === x;
}

var IdUsuario=-1;

var verificarAutentificacion = function(req, resp, next){
	var token=req.body.token || req.query.token || req.headers['x-access-token'];
	if (token) {
    	jwt.verify(token, mySecret, function(err, decoded) {      
		    if (err) {
		        return resp.json({ success: false, message: 'Failed to authenticate token.' });    
		    }else {
		    	IdUsuario=jwt.decode(token, mySecret).id;
				next();
		    }
		});
	} else {
    	return resp.status(403).send({ 
	        success: false, 
	        message: 'No token provided.' 
	    });
  	}
}

var verificarPertenencia = function(req, resp, next){
	var id=req.params.id;
	return models.Mensaje.findById(id).then(function(mensaje){
		if(mensaje)
			if(IdUsuario == mensaje.de){
			  	next();
			}else{
				resp.status(403);
				resp.send('No autorizado'+IdUsuario+' '+mensaje.de);
			}
		else{
			resp.status(404);
			resp.send('No encontrado');

		}

	})
}

// Ver mensajes de un usuario (el usuario puede ser tanto origen como destinatario )
router.post('/leido/:id',verificarAutentificacion, function(req, res) {
	var id=req.params.id;
	return models.Mensaje.findById(id).then(function(mensaje){
		if(mensaje)
			if(IdUsuario == mensaje.para){
			  	  	res.status(200);
			  		mensaje.visto=true;
			  		mensaje.save()
			  		res.send(mensaje)
			}else{

				res.status(403);
				res.send('No autorizado'+IdUsuario+' '+mensaje.de);
			}
		else{
			res.status(404);
			res.send('No encontrado');
		}
	})
});


			 	


// Ver mensajes de un usuario (el usuario puede ser tanto origen como destinatario )
router.get('/',verificarAutentificacion, function(req, res) {
	if(req.query.limit<1 || req.query.limit>20 || !isInteger(req.query.limit) )
		limite=20;
	else
		limite=req.query.limit;
	var offset=req.query.offset;
	if(!(isInteger(offset)))
		offset=0;
	res.status(200);
	var q="SELECT `texto`,`Mensajes`.`id`, `Mensajes`.`createdAt`, `visto`, `de`, `para`, `U1`.`nombre` AS `ParaNombre`, `U2`.`nombre` AS `DeNombre` FROM `Mensajes`"+
	" LEFT OUTER JOIN `Usuarios` AS `U1` ON `para` = `U1`.`id`"+
	" LEFT OUTER JOIN `Usuarios` AS `U2` ON `de` = `U2`.`id`"+
	"WHERE de = "+ IdUsuario +" OR para = "+ IdUsuario +
	" LIMIT "+limite+" OFFSET "+offset+";"
	models.sequelize.query(q,{ type: Sequelize.QueryTypes.SELECT})
	.then(function(projects) {
		res.send(projects)
	})
});


// Ver mensajes no leidos de un usuario (el usuario puede ser tanto origen como destinatario )
router.get('/sinleer/',verificarAutentificacion, function(req, res) {
	if(req.query.limit<1 || req.query.limit>20 || !isInteger(req.query.limit) )
		limite=20;
	else
		limite=req.query.limit;
	var offset=req.query.offset;
	if(!(isInteger(offset)))
		offset=0;
	res.status(200);
	//console.log(Sequelize);
	var q="SELECT `texto`, `fecha`,`Mensajes`.`id`, `Mensajes`.`createdAt`,`visto`, `de`, `para`, `U1`.`nombre` AS `ParaNombre`, `U2`.`nombre` AS `DeNombre` FROM `Mensajes`"+
	" LEFT OUTER JOIN `Usuarios` AS `U1` ON `para` = `U1`.`id`"+
	" LEFT OUTER JOIN `Usuarios` AS `U2` ON `de` = `U2`.`id`"+
	"WHERE (visto != '1' OR visto IS NULL ) AND para = "+ IdUsuario +
	" LIMIT "+limite+" OFFSET "+offset+";"
	models.sequelize.query(q,{ type: Sequelize.QueryTypes.SELECT})
	.then(function(projects) {
		res.send(projects)
  		//console.log(projects)
	})
});


// Ver mensajes enviados
router.get('/enviados/',verificarAutentificacion, function(req, res) {
	if(req.query.limit<1 || req.query.limit>20 || !isInteger(req.query.limit) )
		limite=20;
	else
		limite=req.query.limit;
	var offset=req.query.offset;
	if(!(isInteger(offset)))
		offset=0;
	res.status(200);
	//console.log(Sequelize);
	var q="SELECT `texto`, `fecha`, `visto`, `de`, `para`, `U1`.`nombre` AS `ParaNombre`, `U2`.`nombre` AS `DeNombre` FROM `Mensajes`"+
	" LEFT OUTER JOIN `Usuarios` AS `U1` ON `para` = `U1`.`id`"+
	" LEFT OUTER JOIN `Usuarios` AS `U2` ON `de` = `U2`.`id`"+
	"WHERE de = "+ IdUsuario +
	" LIMIT "+limite+" OFFSET "+offset+";"
	models.sequelize.query(q,{ type: Sequelize.QueryTypes.SELECT})
	.then(function(projects) {
		res.send(projects)
  		//console.log(projects)
	})
});


// Ver mensajes recibidos
router.get('/enviados/',verificarAutentificacion, function(req, res) {
	if(req.query.limit<1 || req.query.limit>20 || !isInteger(req.query.limit) )
		limite=20;
	else
		limite=req.query.limit;
	var offset=req.query.offset;
	if(!(isInteger(offset)))
		offset=0;
	res.status(200);
	//console.log(Sequelize);
	var q="SELECT `texto`, `fecha`, `visto`, `de`, `para`, `U1`.`nombre` AS `ParaNombre`, `U2`.`nombre` AS `DeNombre` FROM `Mensajes`"+
	" LEFT OUTER JOIN `Usuarios` AS `U1` ON `para` = `U1`.`id`"+
	" LEFT OUTER JOIN `Usuarios` AS `U2` ON `de` = `U2`.`id`"+
	"WHERE para = "+ IdUsuario +
	" LIMIT "+limite+" OFFSET "+offset+";"
	models.sequelize.query(q,{ type: Sequelize.QueryTypes.SELECT})
	.then(function(projects) {
		res.send(projects)
  		//console.log(projects)
	})
});


// Añadir mensajes
router.post('/',verificarAutentificacion, function(req, res) {
  	res.status(201);
  	req.body.de=IdUsuario;
  	models.Usuario.findById(req.body.para).then(function(usuario){
  		if(usuario){
			models.Mensaje.create(req.body).then(function(mensaje){
		 		/*var server  = email.server.connect({
					   user:    "mjazorin@azendi.es", 
					   password:"Campello15", 
					   host:    "smtp.1and1.es", 
					   ssl:     true
					});


					server.send({
					   text:    "i hope this works", 
					   from:    "mjazorin@azendi.es", 
					   to:      "jorgeazorin@gmail.com",
					   subject: "testing emailjs"
					}, function(err, message) { console.log(err || message); });*/
		 		res.send(mensaje);
			});
  		}else{
  			res.status(404);
  			res.send("Usuario no encontrado")
  		}

  	})
 	
});

// Actualizar mensajes
router.put('/:id',verificarAutentificacion,verificarPertenencia, function(req, res) {
  	res.status(200);
  	models.Mensaje.findById(req.params.id).then(function(mensaje){
  		mensaje.update(req.body).then(function(mensaje){


  			
 			res.send(mensaje);
  		})
  	});
			 	
});

// Borrar mensajes
router.delete('/:id',verificarAutentificacion ,verificarPertenencia,function(req, res) {
	res.status(204);
  	models.Mensaje.findById(req.params.id).then(function(mensaje){
		mensaje.destroy();
	});
	res.send('');	
});


module.exports = router;