var Sequelize = require('sequelize');
var models = require("../models");
var express = require('express');
var router = express.Router();
var session = require('express-session');
var jwt = require('jsonwebtoken');
var mySecret = "2a4Hj423rmtjsOIsAAAkj";
var unirest = require('unirest');

function isInteger(value) {
  var x = parseFloat(value);
  return !isNaN(value) && (x | 0) === x;
}

var IdUsuario=-1;
var tipo=-1;

var verificarAutentificacion = function(req, resp, next){
	var token=req.body.token || req.query.token || req.headers['x-access-token'];
	if (token) {
    	jwt.verify(token, mySecret, function(err, decoded) {      
		    if (err) {
		        return resp.json({ success: false, message: 'Failed to authenticate token.' });    
		    }else {
		    	IdUsuario=jwt.decode(token, mySecret).id;
		    	tipo=jwt.decode(token,mySecret).tipo;
				next();
		    }
		});
	} else {
    	return resp.status(403).send({ 
	        success: false, 
	        message: 'No token provided.' 
	    });
  	}
}
var verificarPertenencia = function(req, resp, next){
	var id=req.params.id;
	return models.Documento.findById(id).then(function(documento){
		if(documento)
			if(IdUsuario == documento.UsuarioId){
			  	next();
			}else{
				resp.status(403);
				resp.send('No autorizado'+IdUsuario+' '+mensaje.de);
			}
		else{
			resp.status(404);
			resp.send('No encontrado');

		}

	})
}
//Ver documentos de un Usuario
router.get('/',verificarAutentificacion, function(req, res) {

	var q=	" SELECT `Documento`.`id`, `Documento`.`nombre`, `Documento`.`texto`, `Documento`.`URL`, `Documento`.`createdAt`, "+
	"`Documento`.`updatedAt`,`Usuario2`.`ID` AS `propietarioId`,  `Usuario2`.`nombre` AS `propietarioNombre`, `Usuario1`.`UsuarioId`  AS `compartidoid` , "+
	"`Usuario1`.`nombre` AS `compartidoNombre` , `Usuario1`.`id` AS `compartidoId` "+
	" FROM `Documentos` AS `Documento` "+
	"INNER JOIN `Usuarios` AS `Usuario2` ON `Documento`.`propietario` = `Usuario2`.`id` "+
	"LEFT OUTER JOIN `DocumentoUsuarios` AS `DocumentoUsuarios` ON  `Documento`.`id` = `DocumentoUsuarios`.`DocumentoId` "+
	"LEFT OUTER JOIN `Usuarios` AS `Usuario1` ON  `DocumentoUsuarios`.`UsuarioId` = `Usuario1`.`id` "+
	"WHERE compartidoNombre = "+IdUsuario + " OR propietarioId ="+  IdUsuario;
	models.sequelize.query(q,{ type: Sequelize.QueryTypes.SELECT}).then(function(results2){
		res.status(200);
		res.send(results2);	
	})
})


// Añadir documento
router.post('/',verificarAutentificacion, function(req, res) {
  	res.status(201);
  	req.body.propietario=IdUsuario;
 	models.Documento.create(req.body).then(function(documento){
 		res.send(documento);
	});
});


router.post('/compartidos/:id',verificarAutentificacion ,function(req, res) {
	var id=req.params.id;
	models.Usuario.findById(IdUsuario).then(function(doc){
		models.Documento.findById(req.params.id).then(function(user){
			doc.addDocumentoUsuario(user);
			res.status(202);
			res.send('ok');
		})
	})
});




router.delete('/:id',verificarAutentificacion,verificarPertenencia ,function(req, res) {
	var id=req.params.id;
	return models.Documento.findById(id).then(function(documento){
		if(documento)
			if(id == documento.UsuarioId){
			  	documento.destroy();
			  	res.status(204);
				res.send('')
			}else{
				//borrar de la lista de compartidos
				models.Usuario.findById(IdUsuario).then(function(usuario){
					usuario.deleteDocumentoUsuario(documento).then(function(){
						res.status(204);
						res.send('')
					})
				})
			}
		else{
			resp.status(404);
			resp.send('No encontrado');

		}
	})			
});




module.exports = router;