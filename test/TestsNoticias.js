var assert = require('assert');
var practica1 = require('../practica1');
var supertest = require('supertest');
var models = require("../models");
var Sequelize = require('sequelize');

describe('Tests de noticias', function(){
	this.timeout(50000);
	before(function (done) {
		models.sequelize.sync({force:true})
		.then(function(){
		    models.Cliente.create({nombre:'Cliente1', fecha_nac:'1980-1-1'})
		}).then(function(){
		    models.Cliente.create({nombre:'Cliente2', fecha_nac:'1989-1-1'})
		}).then(function(){
    		models.Asesor.create({nombre:'Asesor1', fecha_nac:'1980-1-1'});
		}).then(function() {
    		models.Asesor.create({nombre:'Asesor2', fecha_nac:'1990-10-10'}); 
		}).then(function(){
		    models.Noticia.create({texto:'Noticia1', fecha:'1980-1-1'})
		}).then(function(){
		    models.Noticia.create({texto:'Noticia2', fecha:'1989-1-1'})
		}).then(function(){
    		models.Noticia.create({texto:'Noticia3', fecha:'1980-1-1'});
		}).then(function() {
    		models.Noticia.create({texto:'Noticia4', fecha:'1990-10-10'}); 
		}).then(function(){
			    done();
		})
	})
					

    it('/ Devuelve las noticias', function(done){
		supertest(practica1)
	    .get('/api/noticias/')
	    .expect(function(respuesta){assert(respuesta.text.indexOf('Noticia1')!=-1);})
	    .expect(function(respuesta){assert(respuesta.text.indexOf('Noticia2')!=-1);})
	    .end(done);
	});

	it('/ Devuelve las noticias con limites', function(done){
		supertest(practica1)
	    .get('/api/noticias?limit=1&offset=1')
	    .expect(function(respuesta){assert(respuesta.text.indexOf('Noticia1')==-1);})
	    .expect(function(respuesta){assert(respuesta.text.indexOf('Noticia2')!=-1);})
	    .end(done);
	});

	it('/ Devuelve la noticia 1', function(done){
		supertest(practica1)
	    .get('/api/noticias/2')
	    .expect(function(respuesta){assert(respuesta.text.indexOf('Noticia1')==-1);})
	    .expect(function(respuesta){assert(respuesta.text.indexOf('Noticia2')!=-1);})
	    .end(done);
	});

	it('/ Elimina noticia 2', function(done){
		supertest(practica1)
	    .delete('/api/noticias/2')
	    .end(function(){
	    	supertest(practica1)
	    	.get('/api/noticias/2')
		    .expect(404, done);
	    });
	});

	it('post noticia 2', function(done){
		supertest(practica1)
	    .post('/api/noticias')
	    .send({"texto":"hola jeje"})
	    .end(function(){
	    	supertest(practica1)
	    	.get('/api/noticias')
		    .expect(function(respuesta){assert(respuesta.text.indexOf('hola jeje')!=-1);})
		    .expect(200, done)
	    });
	});

	it('put noticias 1', function(done){
		supertest(practica1)
	    .put('/api/noticias/1')
	    .send({"texto":"hola jeje"})
	    .end(function(){
	    	supertest(practica1)
	    	.get('/api/noticias')
		    .expect(function(respuesta){assert(respuesta.text.indexOf('Noticia1')==-1);})
		    .expect(function(respuesta){assert(respuesta.text.indexOf('hola jeje')!=-1);})
		    .expect(200, done)

		})
	});
 });   
