var assert = require('assert');
var practica1 = require('../practica1');
var supertest = require('supertest');
var models = require("../models");
var Sequelize = require('sequelize');

describe('Tests de documentos', function(){
	this.timeout(50000);
	before(function (done) {
		models.sequelize.sync({force:true})
		.then(function(){
		    models.Cliente.create({nombre:'Cliente1', fecha_nac:'1980-1-1'})
		}).then(function(){
		    models.Cliente.create({nombre:'Cliente2', fecha_nac:'1989-1-1'})
		}).then(function(){
    		models.Asesor.create({nombre:'Asesor1', fecha_nac:'1980-1-1'});
		}).then(function() {
    		models.Asesor.create({nombre:'Asesor2', fecha_nac:'1990-10-10'}); 
		}).then(function() {
			 models.Mensaje.create({texto:'men1', AsesorId:'1',ClienteId:'1'}); 
		}).then(function() {
			 models.Mensaje.create({texto:'men2', AsesorId:'2',ClienteId:'1'}); 
		}).then(function() {
			 models.Documento.create({nombre:'doc1', fecha:'1990-10-10', AsesorId:'1',ClienteId:'1'}); 
		}).then(function() {
		    models.Documento.create({nombre:'doc2', fecha:'1990-10-10', AsesorId:'2',ClienteId:'2'}); 
		}).then(function() {
		    models.Asesor.findById(1).then(function(asesor){
		   		models.Cliente.findById(1).then(function(cliente){
					asesor.addCliente(cliente);
				})
			})
		}).then(function() {
		   models.Asesor.findById(2).then(function(asesor){
		   		models.Cliente.findById(2).then(function(cliente){
					 asesor.addCliente(cliente);
				})
			})
		}).then(function(){
			    done();
		})
	})
					

    it('/ Devuelve los documentos', function(done){
		supertest(practica1)
	    .get('/api/documentos/')
	    .expect(function(respuesta){assert(respuesta.text.indexOf('doc1')!=-1);})
	    .expect(function(respuesta){assert(respuesta.text.indexOf('doc2')!=-1);})
	    .end(done);
	});

	it('/ Devuelve los documentos con limites', function(done){
		supertest(practica1)
	    .get('/api/documentos?limit=1&offset=1')
	    .expect(function(respuesta){assert(respuesta.text.indexOf('doc1')==-1);})
	    .expect(function(respuesta){assert(respuesta.text.indexOf('doc2')!=-1);})
	    .end(done);
	});

	it('/ Devuelve el documento 1', function(done){
		supertest(practica1)
	    .get('/api/documentos/2')
	    .expect(function(respuesta){assert(respuesta.text.indexOf('doc1')==-1);})
	    .expect(function(respuesta){assert(respuesta.text.indexOf('doc2')!=-1);})
	    .end(done);
	});

	it('/ Elimina documento 2', function(done){
		supertest(practica1)
	    .delete('/api/documentos/2')
	    .end(function(){
	    	supertest(practica1)
	    	.get('/api/documentos/2')
		    .expect(404, done);
	    });
	});

	it('post documento 2', function(done){
		supertest(practica1)
	    .post('/api/documentos')
	    .send({"nombre":"cameron"})
	    .end(function(){
	    	supertest(practica1)
	    	.get('/api/documentos')
		    .expect(function(respuesta){assert(respuesta.text.indexOf('cameron')!=-1);})
		    .expect(200, done)
	    });
	});

	it('put documento 1', function(done){
		supertest(practica1)
	    .put('/api/documentos/1')
	    .send({"nombre":"cameron"})
	    .end(function(){
	    	supertest(practica1)
	    	.get('/api/documentos')
		    .expect(function(respuesta){assert(respuesta.text.indexOf('doc1')==-1);})
		    .expect(function(respuesta){assert(respuesta.text.indexOf('cameron')!=-1);})
		    .expect(200, done)

		})
	});
 });   
