"use strict";

module.exports = function(sequelize, DataTypes) {
	var Usuario = sequelize.define('Usuario', {
    login: DataTypes.STRING,
    password: DataTypes.STRING,
    nombre: DataTypes.STRING,
    fecha_nac: DataTypes.DATEONLY,
    dni: DataTypes.STRING,
    info: DataTypes.TEXT,
    tipo: DataTypes.INTEGER
  },{
    classMethods: {
      associate: function(models) {
        Usuario.belongsToMany(models.Documento, {through: 'DocumentoUsuarios', as: 'DocumentoUsuarios', constraints: false });        //, { as: 'ProfilePicture', constraints: false }
        Usuario.hasMany(Usuario, { targetKey: 'asesor'});  
        models.Noticia.belongsTo(Usuario);    
        models.Documento.belongsTo(Usuario, {foreignKey: 'propietario'});
  

      }
    }
  });
  return Usuario;
};