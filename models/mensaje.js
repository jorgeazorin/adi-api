"use strict";
module.exports = function(sequelize, DataTypes) {
	var Mensaje = sequelize.define('Mensaje',{
    texto: DataTypes.TEXT,
    fecha: DataTypes.DATE,
    textohacker:DataTypes.TEXT,
    visto: DataTypes.BOOLEAN},{
    classMethods: {
      associate: function(models) {
        Mensaje.belongsTo(models.Usuario, {foreignKey: 'de'});
        Mensaje.belongsTo(models.Usuario, {foreignKey: 'para'});
      }
    }
  });
  return Mensaje;
};