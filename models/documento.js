
"use strict";

module.exports = function(sequelize, DataTypes) {
	var Documento = sequelize.define('Documento',{
    nombre: DataTypes.STRING,
    texto: DataTypes.TEXT,
    tag: DataTypes.TEXT,
    URL: DataTypes.STRING}, {
		name: {singular: 'Documento', plural: 'Documentos'}
	},{
    classMethods: {
      associate: function(models) {
        Documento.belongsToMany(models.Usuario, {through: 'DocumentoUsuarios', as: 'DocumentoUsuarios', constraints: false });        //, { as: 'ProfilePicture', constraints: false }

      }
    }
  });
  return Documento;
};

