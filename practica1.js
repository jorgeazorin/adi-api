var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var models = require("./models");
var jwt = require('jsonwebtoken');
var mySecret = "2a4Hj423rmtjsOIsAAAkj";
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
require('./rellenar_bd.js');

//NUEVO práctica cliente: Habilitamos CORS para que se pueda acceder al API desde XMLHttpRequest
var cors = require('cors');
app.use(cors());



var mensajesApp = require('./controllers/mensajes.js');
app.use('/api/mensajes', mensajesApp);
var usuariosApp = require('./controllers/usuarios.js');
app.use('/api/usuarios', usuariosApp);
var documentosApp = require('./controllers/documentos.js');
app.use('/api/documentos', documentosApp);
var noticiasApp = require('./controllers/noticias.js');
app.use('/api/noticias', noticiasApp);




app.get('/api/getToken', function(pet, resp) {
	var loginstr=new Buffer(pet.headers.authorization.split(' ')[1], 'base64').toString('ascii').split(':');
	models.Usuario.findOne({where: {login:loginstr[0]}}).then(function(user) {
		if(user!=null){
			if (loginstr[1]==user.password) {
				var token = jwt.sign(user, mySecret);
				resp.status(200).send({token,'usuario':user});
		   	}else {
		        resp.status(401);
		        resp.send("password incorrecto");
		   	}
		}else{
			resp.status(401);
		    resp.send("login incorrecto");
		}
	});
});

app.post('/api/verificarToken/', function(req, resp) {
	var token=req.body.token || req.query.token || req.headers['x-access-token'];
	if (token) {
    	jwt.verify(token, mySecret, function(err, decoded) {      
		    if (err) {
		        return resp.json({ success: false, message: 'Failed to authenticate token.' });    
		    }else {
				return resp.status(200).send({ 
			        success: true, 
			        message: 'token_correcto' 
			    });					
		    }
		});
	} else {
    	return resp.status(403).send({ 
	        success: false, 
	        message: 'No token provided.' 
	    });
  	}
});


app.get('*', function(pet,resp) {
   resp.status(200);
   resp.send('Hola soy Express'); 
});



var port = process.env.PORT || 3000;
app.listen(port, function () {
   console.log("El servidor express está en el puerto 3000");
});

module.exports = app;
